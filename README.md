Docker image for bitcore testnet node

# To build it execute following in the same directory as Dockerfile:
sudo docker build . --tag bitcore_testnet_insight_api_ui

# To run it execute:
sudo docker run --name bitcore_testnet_insight_api_ui_8081 -p 8081:3001 -d bitcore_testnet_insight_api_ui

# Notes
1. This image uses docker Volume to store data as livenet require ~250GB of disk space. For such cases Docker Volume is a best practice
  
  a. volume is created automaticaly so to see which containers use specific volume execute:
  
     docker ps -a --filter volume=VOLUME_NAME_OR_MOUNT_POINT
  
  b. don't forget to remove unneeded volumes after removal of the container. Otherwise volumes will have quickly eaten all your free disk space after several runs as volumes contain blockchain

2. Insight UI is on localhost:8081/insight/

3. Insight API is on localhost:8081/insight-api/

4. bitcore@4.1.0 is used to avoid problem (tbd: insert link)

5. you can attach to running container:
   
   sudo docker exec -it bitcore_testnet_insight_api_ui_8081 bash
   
   and see bitcore debug logs:
   
   tail -f data/testnet3/debug.log
