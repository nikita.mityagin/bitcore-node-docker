FROM node:4.2.6

ENV work_dir ~/wallet

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y libzmq3-dev build-essential
RUN npm -g install bitcore@4.1.0

# Make --testnet as parameter of this file
RUN bitcore create ${work_dir} --testnet && cd ${workdir}
RUN cd ${work_dir} && bitcore install insight-api insight-ui

WORKDIR ${work_dir}
VOLUME ${work_dir}

RUN npm install

CMD bitcored

